# == Sessions ==

# Prefix
set -g prefix C-Space
unbind C-b

# Delay between prefix and command
set -s escape-time 0

# Vi-like bindings for navigation and selection
set-window-option -g mode-keys vi

# Enable/disable mouse control (clickable windows, panes, resizable panes)
set -g mouse off

# Tmux has trouble figuring out the number of colours available in terminal
set -g default-terminal "tmux-256color"
#set -g default-terminal "xterm-256color"

# Persistent SSH key management across sessions
set -g update-environment "DISPLAY SSH_ASKPASS SSH_AUTH_SOCK SSH_AGENT_PID SSH_CONNECTION WINDOWID XAUTHORITY"

# Scrollback history
set -g history-limit 10000

# Easy config reload
bind-key r source-file ~/.tmux.conf \; display-message "~/.tmux.conf reloaded."

# == Windows and Panes ==

# Activity auto-renames window
setw -g automatic-rename on

# Alt-arrow keys to switch panes
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# Shift arrow to switch windows
bind -n S-Left previous-window
bind -n S-Right next-window

# Easier window split keys
bind-key v split-window -h
bind-key h split-window -v

# Toggle between last and current window
bind -n M-t last-window

# == Status Bar ==

# Status line colours
set -g status-bg black
set -g status-fg white

# Window list colours
set -g window-status-style bg=default,fg=white
set -g window-status-current-style bg=yellow,fg=black,bright

# Status bar left
set -g status-left-length 20
set -g status-left ""
set -g status-interval 10
set -g status-justify right

# Status bar right
set -g status-right "[#S] #[fg=blue,bold]#(cat /proc/loadavg | awk '{print $1,$2,$3}')"

# Activity alerts
setw -g monitor-activity on
set -g visual-activity on
