# dotfiles

Configuration files in my **$HOME**.

## AUTHOR

[Daniel Wayne Armstrong](https://www.dwarmstrong.org)

## LICENSE

GPLv3. See [LICENSE](https://gitlab.com/dwarmstrong/dotfiles/-/blob/master/LICENSE.md) for more details.
