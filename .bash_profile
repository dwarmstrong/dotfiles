# ~/.bash_profile: executed by the command interpreter for login shells

# non-login shells
source $HOME/.bashrc

# set search path for programs
if [ -d $HOME/.local/bin ] ; then
  PATH=$HOME/.local/bin:${PATH}
fi
if [ -d $HOME/bin ] ; then
  PATH=$HOME/bin:${PATH}
fi
export PATH

# manage multiple versions of Python using pyenv
if [ -d "$HOME/.pyenv" ] ; then
    export PYENV_ROOT="$HOME/.pyenv"
    command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
fi
