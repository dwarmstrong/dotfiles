function l --wraps='colorls -alFG' --description 'alias l=colorls -alFG'
  colorls -alFG $argv
end
