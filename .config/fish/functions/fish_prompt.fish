function fish_prompt
  # save the return status of the previous command
  set -l last_status $status
  # display prompt status only if it's not 0
  set -l stat
  if test $last_status -ne 0
    set stat (set_color brred)"[$last_status]"(set_color normal)
  end
  # if accessing via ssh include $ssh_session message
  set -l ssh_session
  if set -q SSH_TTY
    set ssh_session (set_color brred)"-ssh_session"(set_color normal)
  end

  # set a two-line prompt 
  set_color brmagenta
  printf '%s' $(date "+%H:%M:%S")
  set_color brgreen
  printf ' %s' $USER
  set_color brwhite
  printf ' at'
  set_color bryellow
  printf ' %s%s' (prompt_hostname) $ssh_session
  set_color brwhite
  printf ' in'
  set_color brblue
  printf ' %s %s' (prompt_pwd --dir-length=0)  $stat
  set_color normal
  printf '\n> '
end
