if status is-interactive
  # Commands to run in interactive sessions can go here
  fish_add_path -p $HOME/bin /usr/sbin /sbin /usr/X11R7/bin
  abbr -a dff df -h
  abbr -a e nvim
  abbr -a gsave git commit -m 'save'
  abbr -a gs git status
  abbr -a p less
  abbr -a t c ~/tmp
  abbr -a tmuxd tmux new -s default -A
  abbr -a x exit
end
